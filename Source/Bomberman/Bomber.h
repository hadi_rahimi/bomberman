// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Bomb.h"
#include "Bomber.generated.h"

class ALevelCreator;
class UGameUi;

UCLASS()
class BOMBERMAN_API ABomber : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABomber();
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Me")
		int player;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Me")
		float speed;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Me")
		int lives;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Me")
		int bombs;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Me")
		int bombRange;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Me")
		TSubclassOf<ABomb> bombType;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	AMyPlayerController*	m_pMyPlayerController;
	float					m_fSpeedFactor;
	int						m_iBombs;
	int						m_iBombRange;
	int						m_iScore;

	bool					m_bLastMoveDirWasX;
	bool					m_bLastMoveDirWasY;

	UGameUi* GetGameUi();
	void ResetGame();
	void SnapToNearestCorridor(FVector& ptCurPos, FVector ptCurTileCenter, bool bSnapX, bool bSnapY);

	virtual void OnConstruction(const FTransform& Transform);
	virtual void Tick(float DeltaTime) override;
	virtual void PlaceBomb();
	virtual void NotifyActorBeginOverlap(AActor* OtherActor);

};
