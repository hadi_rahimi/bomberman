// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "Wall.h"
#include "FixedWall.h"
#include "MyPlayerController.generated.h"

class UGameUi;
class ABomber;

/**
 * 
 */
UCLASS()
class BOMBERMAN_API AMyPlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Me")
		TSubclassOf<AFixedWall> FixedWallType;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Me")
		TSubclassOf<AWall>		WallType;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Me")
		TSubclassOf<ABomber>	bomberType1;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Me")
		TSubclassOf<ABomber>	bomberType2;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Me")
		TSubclassOf<UGameUi>	gameUiType;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Me")
		int gameTime;
	UPROPERTY(BlueprintReadWrite)
		UGameUi* gameUi;
	UFUNCTION(BlueprintCallable, Category = "Me")
		void ResetGame();

public:
	int				m_iGameTimer;
	FTimerHandle	m_TimerHandle;

	ABomber*		bomber1;
	ABomber*		bomber2;

	UGameUi*		m_pGameUiCpp;
	bool			m_bGameEnded;

	void CreateWallsFromMap();
	void OneSecondElapsed();
	void FinishGame(const char* szShowMsg);
	void EnableTimer(bool bEnable);
	UGameUi* GetGameUi() { return m_pGameUiCpp; }

	virtual void OnConstruction(const FTransform & Transform);
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;
};
