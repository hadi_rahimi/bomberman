// Fill out your copyright notice in the Description page of Project Settings.

#include "MyPlayerController.h"
#include "Global.h"
#include "Common.h"
#include "GameUi.h"
#include "Bomber.h"
#include <Kismet/GameplayStatics.h>

#pragma optimize("", off)


void AMyPlayerController::OnConstruction(const FTransform & Transform)
{
	if(gameTime == 0)
		gameTime = 300;
}

void AMyPlayerController::EnableTimer(bool bEnable)
{
	if (bEnable) {
		if (m_TimerHandle.IsValid())
			GetWorld()->GetTimerManager().ClearTimer(m_TimerHandle);
		m_TimerHandle.Invalidate();
		GetWorld()->GetTimerManager().SetTimer(m_TimerHandle, this, &AMyPlayerController::OneSecondElapsed, 1.0f, true);
	}
	else {
		GetWorld()->GetTimerManager().ClearTimer(m_TimerHandle);
		m_TimerHandle.Invalidate();
	}
}

void ResetBuffer(void* buffer[TILESX][TILESY])
{
	for (int x = 0; x < TILESX; x++) {
		for (int y = 0; y < TILESY; y++)
			buffer[x][y] = NULL;
	}
}

void DestroyActors(void* actors[TILESX][TILESY])
{
	for (int x = 0; x < TILESX; x++) {
		for (int y = 0; y < TILESY; y++) {
			AActor* pActor = (AActor*)actors[x][y];
			if (pActor) {
				if(!pActor->IsPendingKill())
					pActor->Destroy();
			}
			actors[x][y] = NULL;
		}
	}
}

void ShowWalls()
{
	for (int x = 0; x < TILESX; x++) {
		for (int y = 0; y < TILESY; y++) {
			if (g_map[x][y] == OBJTYPE_WALL) {
				AWall* pWall = (AWall*)g_walls[x][y];
				if (pWall)
					pWall->SetActorHiddenInGame(false);
			}
		}
	}
}

void AMyPlayerController::ResetGame()
{
	m_bGameEnded = false;

	// Reset players position
	bomber1->ResetGame();
	bomber2->ResetGame();

	// Reset walls
	ResetMapBuffer();

	// Show destructible walls again
	ShowWalls();

	DestroyActors(g_bombs);
	DestroyActors(g_pickups);

	//if (bomber1)
	//	bomber1->m_pMyPlayerController = this;
	//if (bomber2)
	//	bomber2->m_pMyPlayerController = this;

	// Reset timer
	m_iGameTimer = gameTime;

	// Clear and start new timer for game
	EnableTimer(true);

	// Hide end dialog
	if(GetGameUi())
		GetGameUi()->SetEndGameDlgVisiblity(false);
	
	UGameplayStatics::SetGamePaused(this, false);
}

void AMyPlayerController::OneSecondElapsed()
{
	m_iGameTimer--;
	if (m_iGameTimer >= 0) {
		int iMin = m_iGameTimer / 60;
		int iSec = m_iGameTimer % 60;

		char s[50];
		sprintf_s(s, sizeof(s), "%02d:%02d", iMin, iSec);
		if(GetGameUi())
			GetGameUi()->SetTime(s);
	}
	else {
		FinishGame("GAME OVER");
	}
}

void AMyPlayerController::FinishGame(const char* szShowMsg)
{
	UGameplayStatics::SetGamePaused(this, true);

	if (m_bGameEnded)
		return;

	m_bGameEnded = true;

	// Timeout
	//UWidgetBlueprintLibrary::SetInputMode_UIOnly(this, gameUi);
	//bEnableMouseOverEvents = true;
	//SetInputMode(FInputModeUIOnly());

	// Stop timer
	EnableTimer(false);

	// Show end dialog
	if (GetGameUi()) {
		GetGameUi()->SetEndGameDlgText(szShowMsg);
		GetGameUi()->SetEndGameDlgVisiblity(true);
	}
}

void AMyPlayerController::BeginPlay()
{
	Super::BeginPlay();

	bShowMouseCursor = true;

	ResetBuffer(g_walls);
	ResetBuffer(g_bombs);
	ResetBuffer(g_pickups);

	// Create walls
	CreateWallsFromMap();

	if (1) {
		FInputModeGameAndUI mode;
		mode.SetLockMouseToViewportBehavior(EMouseLockMode::DoNotLock);
		mode.SetHideCursorDuringCapture(false);
		SetInputMode(mode);
	}

	// Create widget
	m_pGameUiCpp = CreateWidget<UGameUi>(this, gameUiType);
	if (m_pGameUiCpp)
		m_pGameUiCpp->AddToViewport(1);

	// Create players
	FActorSpawnParameters spawnInfo;
	FVector pt(50, 50, 50);
	bomber1 = GetWorld()->SpawnActor<ABomber>(bomberType1, pt, FRotator::ZeroRotator, spawnInfo);
	bomber2 = GetWorld()->SpawnActor<ABomber>(bomberType2, pt, FRotator::ZeroRotator, spawnInfo);

	ResetGame();
}

void AMyPlayerController::CreateWallsFromMap()
{
	ResetMapBuffer();

	UWorld* pWorld = GetWorld();

	int iFixedWallCount = 0;
	int iWallCount = 0;

	for (int y = 0; y < TILESY; y++) {
		for (int x = 0; x < TILESX; x++) {
			float ypos = y * TILESIZE + TILESIZEDIV2;
			float xpos = x * TILESIZE + TILESIZEDIV2;
			FVector pos(xpos, ypos, 50);
			FActorSpawnParameters spawnInfo;
			//spawnInfo.ObjectFlags |= RF_Transient;

			g_walls[x][y] = NULL;

			if (g_map[x][y] == OBJTYPE_FIXEDWALL) {
				pWorld->SpawnActor<AFixedWall>(FixedWallType, pos, FRotator::ZeroRotator, spawnInfo);
			}
			else if (g_map[x][y] == OBJTYPE_WALL) {
				AWall* pWall = pWorld->SpawnActor<AWall>(WallType, pos, FRotator::ZeroRotator, spawnInfo);
				if (pWall) {
					pWall->SetTileIndices(x, y);
					g_walls[x][y] = pWall;
				}
			}

		}//x
	}//y
}

void AMyPlayerController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

