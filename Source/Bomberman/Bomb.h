// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Bomb.generated.h"

class AMyPlayerController;
class UGameUi;
class ABomber;

UCLASS()
class BOMBERMAN_API ABomb : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABomb();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Me")
		int range;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Me")
		float timeout;
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	

	FTimerHandle			m_TimerHandle;
	ABomber*				m_pPlayer;
	AMyPlayerController*	m_pPlayerController;
	bool					m_bExploded;
	int						m_iRange;
	int						m_iTimeout;

	// Called every frame
	virtual void Tick(float DeltaTime) override;
	virtual void Explode();

	void SetPlayer(ABomber* pPlayer);
	void SetPlayerController(AMyPlayerController* pPlayerController);
	void SetRange(int iRange) { m_iRange = iRange; }
	void SetTimeout(int iTimeout) { m_iTimeout = iTimeout; }
	void ResetTimer();
	bool CanSee(int x1, int y1, int x2, int y2);
	bool IsExploded() { return m_bExploded; }
	void CheckPlayerDeath(ABomber* pPlayer);

};
