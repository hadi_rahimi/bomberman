#pragma once

#define SIZEX	1100.0f
#define SIZEY	1300.0f

#define TILESX	11
#define TILESY	13

#define TILESIZE		(SIZEX / TILESX)
#define TILESIZEDIV2	(TILESIZE / 2)

extern int		g_map[TILESX][TILESY];
extern void*	g_walls[TILESX][TILESY]; // Pointer to walls (actors)
extern void*	g_bombs[TILESX][TILESY]; // Pointer to bombs (actors)
extern void*	g_pickups[TILESX][TILESY]; // Pointer to pickups (actors)

enum OBJTYPE 
{
	OBJTYPE_INVALID = -1,
	OBJTYPE_NONE = 0,
	OBJTYPE_FIXEDWALL = 1,
	OBJTYPE_WALL = 2,
	OBJTYPE_BOMB = 3
};