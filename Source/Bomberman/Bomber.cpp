// Fill out your copyright notice in the Description page of Project Settings.

#include "Bomber.h"
#include "Global.h"
#include "Common.h"
#include "EngineUtils.h"
#include "GameUi.h"
#include "MyPlayerController.h"

#pragma optimize("", off)

// Sets default values
ABomber::ABomber()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

void ABomber::OnConstruction(const FTransform& Transform)
{
	Super::OnConstruction(Transform);
}

UGameUi* ABomber::GetGameUi()
{
	return m_pMyPlayerController->GetGameUi();
}

// Called when the game starts or when spawned
void ABomber::BeginPlay()
{
	Super::BeginPlay();

	m_pMyPlayerController = (AMyPlayerController*)GetWorld()->GetFirstPlayerController();

	if (InputComponent) {
		if (player == 0) {
			InputComponent->BindAxis("DirectionX1");
			InputComponent->BindAxis("DirectionY1");
			InputComponent->BindAction("PlaceBomb1", IE_Pressed, this, &ABomber::PlaceBomb);
		}
		else {
			InputComponent->BindAxis("DirectionX2");
			InputComponent->BindAxis("DirectionY2");
			InputComponent->BindAction("PlaceBomb2", IE_Pressed, this, &ABomber::PlaceBomb);
		}
	}
}

bool CheckCollision(const FVector& pt)
{
	bool bCollision = true;

	int iX, iY;
	GetTileIndex(pt, iX, iY);

	if (0 <= iX && iX < TILESX) {
		if (0 <= iY && iY < TILESY) {
			OBJTYPE eType = (OBJTYPE)g_map[iX][iY];
			if (eType == OBJTYPE_NONE || eType == OBJTYPE_BOMB)
				bCollision = false;
		}
	}

	return bCollision;
}

struct MYRECT 
{
	FVector TopLeft;
	FVector TopRight;
	FVector BottomLeft;
	FVector BottomRight;
};

void MyDeflateRect(MYRECT* p, float fValueX, float fValueY)
{
	p->TopLeft.X -= fValueX;
	p->TopLeft.Y += fValueY;
	
	p->TopRight.X -= fValueX;
	p->TopRight.Y -= fValueY;

	p->BottomLeft.X += fValueX;
	p->BottomLeft.Y += fValueY;

	p->BottomRight.X += fValueX;
	p->BottomRight.Y -= fValueY;
}

void CheckSides(const FVector& playerPos, float x, float y, bool& bCollisionX, bool& bCollisionY)
{
	bCollisionX = false;
	bCollisionY = false;

	MYRECT playerRect;
	playerRect.TopLeft = playerPos + FVector(+TILESIZEDIV2, -TILESIZEDIV2, 0); // Top Left
	playerRect.TopRight = playerPos + FVector(+TILESIZEDIV2, +TILESIZEDIV2, 0); // Top Right
	playerRect.BottomLeft = playerPos + FVector(-TILESIZEDIV2, -TILESIZEDIV2, 0); // Bottom Left
	playerRect.BottomRight = playerPos + FVector(-TILESIZEDIV2, +TILESIZEDIV2, 0); // Bottom Right
	
	float fMargin = TILESIZE * 0.25f;

	if (y != 0.0f) { // Moving left/right (along y axis)
		MYRECT r = playerRect;
		MyDeflateRect(&r, fMargin, 0.0f);
		if (y < 0.0f) { // Moving left
			if (CheckCollision(r.TopLeft))
				bCollisionY = true;
			if (CheckCollision(r.BottomLeft))
				bCollisionY = true;
		}
		else { // Moving right
			if (CheckCollision(r.TopRight))
				bCollisionY = true;
			if (CheckCollision(r.BottomRight))
				bCollisionY = true;
		}
	}
	
	if (x != 0.0f) { // Moving up/down (along x axis)
		MYRECT r = playerRect;
		MyDeflateRect(&r, 0.0f, fMargin);
		if (x < 0.0f) { // Moving down
			if (CheckCollision(r.BottomLeft))
				bCollisionX = true;
			if (CheckCollision(r.BottomRight))
				bCollisionX = true;
		}
		else { // Moving up
			if (CheckCollision(r.TopLeft))
				bCollisionX = true;
			if (CheckCollision(r.TopRight))
				bCollisionX = true;
		}
	}
}

void CheckDirection(bool& bAllowX, bool& bAllowY, FVector ptCurPos, FVector ptCurTileCenter)
{
	FVector pos = ptCurPos;

	pos -= ptCurTileCenter;
	pos /= TILESIZE;

	bAllowX = false;
	bAllowY = false;

	float fCrossingRadius = 0.25f;

	if (fabs(pos.X) < fCrossingRadius)
		bAllowY = true;
	if (fabs(pos.Y) < fCrossingRadius)
		bAllowX = true;;
}

void ABomber::SnapToNearestCorridor(FVector& ptCurPos, FVector ptCurTileCenter, bool bSnapX, bool bSnapY)
{
	FVector pt = ptCurPos;
	pt -= ptCurTileCenter;
	pt /= TILESIZE;
	pt.X = round(pt.X);
	pt.Y = round(pt.Y);
	pt *= TILESIZE;
	pt += ptCurTileCenter;

	if(bSnapX)
		ptCurPos.X = pt.X;
	if(bSnapY)
		ptCurPos.Y = pt.Y;
}

bool IsOpen(int iX, int iY)
{
	bool bOpen = false;

	if (0 <= iX && iX < TILESX) {
		if (0 <= iY && iY < TILESY) {
			OBJTYPE eType = (OBJTYPE)g_map[iX][iY];
			if (eType != OBJTYPE_WALL && eType != OBJTYPE_FIXEDWALL)
				bOpen = true;
		}
	}
	return bOpen;
}

// Called every frame
void ABomber::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	float x = 0.0f;
	float y = 0.0f;
	
	if (InputComponent) {
		if (player == 0) {
			x = InputComponent->GetAxisValue(L"DirectionX1");
			y = InputComponent->GetAxisValue(L"DirectionY1");
		}
		else {
			x = InputComponent->GetAxisValue(L"DirectionX2");
			y = InputComponent->GetAxisValue(L"DirectionY2");
		}
	}

	if (x != 0.0f || y != 0.0f)
		x = x;// for debug breakpoint
	if (x != 0.0f && y != 0.0f)
		x = x;// for debug

	float dx = x * DeltaTime * m_fSpeedFactor * speed;
	float dy = y * DeltaTime * m_fSpeedFactor * speed;

	FVector curPos = GetActorLocation();
	FVector temp;

	FVector curTileCenter = GetTileCenter(curPos);

	bool bAllowX = false;
	bool bAllowY = false;
	CheckDirection(bAllowX, bAllowY, curPos, curTileCenter);

	bool bCollisionY = true;
	bool bCollisionX = true;

	FVector newPos = curPos;

	bool bMoveX = false;
	bool bMoveY = false;

	if (x != 0.0f && bAllowX) {
		temp = curPos + FVector(dx, 0, 0);
		bool bDummy;
		CheckSides(temp, x, y, bCollisionX, bDummy);
		if (bCollisionX) { // In collision state, snap to corridor
			FVector pt = curPos;
			SnapToNearestCorridor(pt, curTileCenter, true, false);
			newPos.X = pt.X;
		}
		else
			bMoveX = true;
	}
	if (y != 0.0f && bAllowY) {
		temp = curPos + FVector(0, dy, 0);
		bool bDummy;
		CheckSides(temp, x, y, bDummy, bCollisionY);
		if (bCollisionY) { // In collision state, snap to corridor
			FVector pt = curPos;
			SnapToNearestCorridor(pt, curTileCenter, false, true);
			newPos.Y = pt.Y;
		}
		else
			bMoveY = true;
	}

	if (!bAllowX && !bAllowY) {
		FVector pt = curPos;
		SnapToNearestCorridor(pt, curTileCenter, true, true);
		newPos = pt;
	}

	//if (y != 0.0f) { // Turning left/right
	//	
	//	int iX, iY;
	//	GetTileIndex(curPos, iX, iY);

	//	bool bOpen;
	//	if (y > 0.0f)
	//		bOpen = IsOpen(iX, iY + 1);
	//	else
	//		bOpen = IsOpen(iX, iY - 1);
	//	static float s_f = -1.0f;
	//	if (bOpen) {
	//		if (curPos.X < curTileCenter.X)
	//			dx -= s_f;
	//		else
	//			dx += s_f;
	//		bMoveX = true;
	//	}
	//	
	//	//FVector pt = curPos;
	//	//pt -= curTileCenter;
	//	//pt /= TILESIZE;
	//	//if (pt.X > 0.0f) {
	//	//	
	//	//}

	//}

	if (bMoveX) {
		newPos.X += dx;
		m_bLastMoveDirWasX = true;
	}
	if (bMoveY) {
		newPos.Y += dy;
		m_bLastMoveDirWasY = true;
	}

	SetActorLocation(newPos);
}


void ABomber::PlaceBomb()
{
	GetWorld()->GetFirstPlayerController();

	if (m_iBombs) {

		// Create bomb in player position
		UWorld* pWorld = GetWorld();

		FVector actorPos = GetActorLocation();
		FVector bombPos = GetTileCenter(actorPos);

		FActorSpawnParameters spawnInfo;
		ABomb* pBomb = pWorld->SpawnActor<ABomb>(bombType, bombPos, FRotator::ZeroRotator, spawnInfo);
		pBomb->SetPlayerController(m_pMyPlayerController);
		pBomb->SetPlayer(this);
		pBomb->SetRange(m_iBombRange);
		pBomb->SetTimeout(pBomb->timeout * m_iBombRange + 0.5f);
		pBomb->ResetTimer();

		int iX, iY;
		GetTileIndex(bombPos, iX, iY);
		g_map[iX][iY] = OBJTYPE_BOMB;
		g_bombs[iX][iY] = pBomb;

		m_iBombs--;

		// Show bomb count
		if(GetGameUi())
			GetGameUi()->SetBombs(player, m_iBombs);
	}
}

void ABomber::NotifyActorBeginOverlap(AActor* OtherActor)
{
	Super::NotifyActorBeginOverlap(OtherActor);

	if (OtherActor->IsA(APickup::StaticClass())) {

		APickup* pPickup = (APickup*)OtherActor;
		if (pPickup->GetType() == APickup::PICKUPTYPE_BOMB) {
			m_iBombs++;
			if (GetGameUi())
				GetGameUi()->SetBombs(player, m_iBombs);
		}
		else if (pPickup->GetType() == APickup::PICKUPTYPE_SPEED) {
			m_fSpeedFactor += 0.5f;
			if (m_fSpeedFactor > 2.0f)
				m_fSpeedFactor = 2.0f;
			if (GetGameUi())
				GetGameUi()->SetSpeedFactor(player, m_fSpeedFactor);
		}
		else if (pPickup->GetType() == APickup::PICKUPTYPE_DELAY) {
			m_iBombRange += 1;
			if (GetGameUi())
				GetGameUi()->SetBombRange(player, m_iBombRange);
		}
		m_iScore += 100;
		if (GetGameUi())
			GetGameUi()->SetScore(player, m_iScore);

		// Delete pickup
		pPickup->Delete();
	}
}

void ABomber::ResetGame()
{
	m_fSpeedFactor = 1.0f;
	m_iBombRange = 1;
	m_iBombs = bombs;
	m_iBombRange = bombRange;
	//m_iScore = 0;

	m_bLastMoveDirWasX = false;
	m_bLastMoveDirWasY = false;

	// Reset location
	FVector pt;
	if(player == 0)
		pt = GetTileCenter(TILESX - 1, TILESY - 1);
	else
		pt = GetTileCenter(TILESX - 1, 0);
	SetActorLocation(pt);

	// Show player
	SetActorHiddenInGame(false);

	// Update UI
	if (GetGameUi()) {
		GetGameUi()->SetBombs(player, m_iBombs);
		GetGameUi()->SetBombRange(player, m_iBombRange);
		GetGameUi()->SetSpeedFactor(player, m_fSpeedFactor);
		GetGameUi()->SetScore(player, m_iScore);
	}

}