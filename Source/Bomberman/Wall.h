// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Pickup.h"
#include "Wall.generated.h"

UCLASS()
class BOMBERMAN_API AWall : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWall();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Me")
		TSubclassOf<APickup> pickupBombType;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Me")
		TSubclassOf<APickup> pickupSpeedType;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Me")
		TSubclassOf<APickup> pickupDelayType;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	

	int m_iTileX;
	int m_iTileY;

	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void SetTileIndices(int iX, int iY);
	void Explode();
};
