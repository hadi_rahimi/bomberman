// Fill out your copyright notice in the Description page of Project Settings.

#include "Wall.h"
#include "Global.h"
#include "Common.h"

#pragma optimize("", off)

// Sets default values
AWall::AWall()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	m_iTileX = 0;
	m_iTileY = 0;
}

// Called when the game starts or when spawned
void AWall::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AWall::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AWall::SetTileIndices(int iX, int iY)
{
	m_iTileX = iX;
	m_iTileY = iY;
}

void AWall::Explode()
{
	SetActorHiddenInGame(true);

	int iRand = FMath::RandRange(1, 100);
	if (iRand <= 30) {

		FActorSpawnParameters spawnInfo;
		FVector pos = GetActorLocation();

		APickup* pPickup = NULL;

		int iRand2 = rand() % 3;
		if (iRand2 == 0) {
			pPickup = GetWorld()->SpawnActor<APickup>(pickupBombType, pos, FRotator::ZeroRotator, spawnInfo);
		}
		else if (iRand2 == 1) {
			pPickup = GetWorld()->SpawnActor<APickup>(pickupSpeedType, pos, FRotator::ZeroRotator, spawnInfo);
		}
		else if (iRand2 == 2) {
			pPickup = GetWorld()->SpawnActor<APickup>(pickupDelayType, pos, FRotator::ZeroRotator, spawnInfo);
		}

		int iX, iY;
		GetTileIndex(pos, iX, iY);
		g_pickups[iX][iY] = pPickup;
	}
}
