// Fill out your copyright notice in the Description page of Project Settings.

#include "Bomb.h"
#include "Global.h"
#include "common.h"
#include "Bomber.h"
#include "GameUi.h"
#include "MyPlayerController.h"
#include <vector>

#pragma optimize("", off)

// Sets default values
ABomb::ABomb()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Initial range of bomb is one tile
	m_iRange = range; 
	m_pPlayer = NULL;
}

OBJTYPE EnumNeighbours(int& iX, int& iY, int iDirX, int iDirY, int& iRange)
{
	if (iRange <= 0)
		return OBJTYPE_INVALID;

	iRange--; // Go in direction until range

	iX += iDirX;
	iY += iDirY;
	if (0 <= iX && iX < TILESX) {
		if (0 <= iY && iY < TILESY)
			return (OBJTYPE)g_map[iX][iY];
	}
	return OBJTYPE_INVALID;
}

void ExplodeInDirection(int iX, int iY, int iDirX, int iDirY, int iRange)
{
	int iContext = iRange;
	while (true) {
		OBJTYPE type = EnumNeighbours(iX, iY, iDirX, iDirY, iContext);
		if (type == OBJTYPE_INVALID)
			break;
		if (type == OBJTYPE_FIXEDWALL) // Explosion is blocked by fixed wall
			break;
		if (type == OBJTYPE_WALL) {
			g_map[iX][iY] = 0;
			AWall* pWall = (AWall*)g_walls[iX][iY];
			if (pWall)
				pWall->Explode();
			break; // Explosion is blocked by wall
		}
		else if(type == OBJTYPE_BOMB) { // Bomb
			ABomb* pBomb = (ABomb*)g_bombs[iX][iY];
			if (pBomb) {
					pBomb->Explode();
			}
		}
	}
}

void ABomb::ResetTimer()
{
	if (m_TimerHandle.IsValid())
		GetWorld()->GetTimerManager().ClearTimer(m_TimerHandle);
	m_TimerHandle.Invalidate();
	GetWorld()->GetTimerManager().SetTimer(m_TimerHandle, this, &ABomb::Explode, m_iTimeout, false);
}

bool ABomb::CanSee(int x1, int y1, int x2, int y2)
{
	bool bCanSee = true;
	if (x1 == x2) {
		int iMinY = FMath::Min<int>(y1, y2);
		int iMaxY = FMath::Max<int>(y1, y2);
		for (int y = iMinY + 1; y < iMaxY; y++) {
			OBJTYPE eType = (OBJTYPE)g_map[x1][y];
			if (eType == OBJTYPE_WALL || eType == OBJTYPE_FIXEDWALL) {
				bCanSee = false;
				break;
			}
		}
	}
	else if(y1 == y2) {
		int iMinX = FMath::Min<int>(x1, x2);
		int iMaxX = FMath::Max<int>(x1, x2);
		for (int x = iMinX + 1; x < iMaxX; x++) {
			OBJTYPE eType = (OBJTYPE)g_map[x][y1];
			if (eType == OBJTYPE_WALL || eType == OBJTYPE_FIXEDWALL) {
				bCanSee = false;
				break;
			}
		}
	}

	return bCanSee;
}

void ABomb::CheckPlayerDeath(ABomber* pPlayer)
{
	if (!pPlayer->IsPendingKill()) {
		
		int iPlayerX, iPlayerY;
		int iBombX, iBombY;

		FVector ptPlayer = pPlayer->GetActorLocation();
		GetTileIndex(ptPlayer, iPlayerX, iPlayerY);
		
		FVector ptBomb = GetActorLocation();
		GetTileIndex(ptBomb, iBombX, iBombY);

		if (iBombX == iPlayerX || iBombY == iPlayerY) { // Check horizontal and vertical alignment
			float fDist = FVector::Dist(ptPlayer, ptBomb); // check distance
			if (fDist < (m_iRange + 0.9f) * TILESIZE) {

				bool bCanSee = CanSee(iBombX, iBombY, iPlayerX, iPlayerY);
				if (bCanSee) {
					pPlayer->SetActorHiddenInGame(true);
					if (pPlayer->player == 0)
						m_pPlayerController->FinishGame("WINNER: PLAYER 2");
					else
						m_pPlayerController->FinishGame("WINNER: PLAYER 1");
				}
			}
		}
	}
}

void ABomb::Explode()
{
	timeout = 0.0f;

	// Check players death
	CheckPlayerDeath(m_pPlayerController->bomber1);
	CheckPlayerDeath(m_pPlayerController->bomber2);

	FVector pos = GetActorLocation();
	int iX, iY;
	GetTileIndex(pos, iX, iY);
	
	m_bExploded = true;
	g_map[iX][iY] = OBJTYPE_NONE;
	g_bombs[iX][iY] = NULL;

	ExplodeInDirection(iX, iY, 0,-1, m_iRange);
	ExplodeInDirection(iX, iY, 0,+1, m_iRange);
	ExplodeInDirection(iX, iY,+1, 0, m_iRange);
	ExplodeInDirection(iX, iY,-1, 0, m_iRange);


	// Add Bomb count again
	m_pPlayer->m_iBombs++;
	if (m_pPlayerController) {
		if (m_pPlayerController->GetGameUi())
			m_pPlayerController->GetGameUi()->SetBombs(m_pPlayer->player, m_pPlayer->m_iBombs);
	}

	Destroy();
}

// Called when the game starts or when spawned
void ABomb::BeginPlay()
{
	Super::BeginPlay();
	
	m_bExploded = false;
	m_TimerHandle.Invalidate();
	//GetWorld()->GetTimerManager().SetTimer(m_TimerHandle, this, &ABomb::Explode, timeout, false);
}

// Called every frame
void ABomb::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ABomb::SetPlayer(ABomber* pPlayer)
{
	m_pPlayer = pPlayer;
}

void ABomb::SetPlayerController(AMyPlayerController* pPlayerController)
{
	m_pPlayerController = pPlayerController;
}


