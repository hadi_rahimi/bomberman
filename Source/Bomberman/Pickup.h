// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Pickup.generated.h"

UCLASS()
class BOMBERMAN_API APickup : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APickup();

	enum PICKUPTYPE {
		PICKUPTYPE_BOMB = 0,
		PICKUPTYPE_SPEED = 1,
		PICKUPTYPE_DELAY = 2
	};

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Me")
		int pickupType;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	PICKUPTYPE GetType() { return (PICKUPTYPE)pickupType; }
	void Delete();
};
