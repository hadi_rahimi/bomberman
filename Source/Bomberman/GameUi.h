// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "GameUi.generated.h"

/**
 * 
 */
UCLASS()
class BOMBERMAN_API UGameUi : public UUserWidget
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Me")
		FString cppBombs1;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Me")
		FString cppBombs2;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Me")
		FString cppBombRange1;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Me")
		FString cppBombRange2;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Me")
		FString cppSpeed1;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Me")
		FString cppSpeed2;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Me")
		FString cppScore1;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Me")
		FString cppScore2;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Me")
		FString cppTime;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Me")
		FString cppEndGameDlgText;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Me")
		ESlateVisibility cppEndGameDlgVisiblity;

public:
	UGameUi(const FObjectInitializer& ObjectInitializer);
	void SetBombs(int iPlayer, int iValue);
	void SetBombRange(int iPlayer, int iValue);
	void SetSpeedFactor(int iPlayer, float fValue);
	void SetScore(int iPlayer, int iValue);
	void SetTime(const char *szTime);
	void SetEndGameDlgText(const char *szText);
	void SetEndGameDlgVisiblity(bool bShow);
};
