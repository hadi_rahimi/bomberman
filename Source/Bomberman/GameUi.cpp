// Fill out your copyright notice in the Description page of Project Settings.

#include "GameUi.h"
#include <PanelWidget.h>

UGameUi::UGameUi(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	SetEndGameDlgVisiblity(false);
}

void UGameUi::SetBombs(int iPlayer, int iValue)
{
	char s[50];
	sprintf_s(s, sizeof(s), "BOMBS: %d", iValue);
	if (iPlayer == 0)
		cppBombs1 = s;
	else
		cppBombs2 = s;
}

void UGameUi::SetBombRange(int iPlayer, int iValue)
{
	char s[50];
	sprintf_s(s, sizeof(s), "RANGE: %d", iValue);
	if (iPlayer == 0)
		cppBombRange1 = s;
	else
		cppBombRange2 = s;
}

void UGameUi::SetSpeedFactor(int iPlayer, float fValue)
{
	char s[50];
	sprintf_s(s, sizeof(s), "SPEED: %.1fX", fValue);
	if (iPlayer == 0)
		cppSpeed1 = s;
	else
		cppSpeed2 = s;
}

void UGameUi::SetScore(int iPlayer, int iValue)
{
	char s[50];
	sprintf_s(s, sizeof(s), "SCORE: %d", iValue);
	if (iPlayer == 0)
		cppScore1 = s;
	else
		cppScore2 = s;
}

void UGameUi::SetTime(const char *szTime)
{
	cppTime = szTime;
}

void UGameUi::SetEndGameDlgText(const char *szText)
{
	cppEndGameDlgText = szText;
}

void UGameUi::SetEndGameDlgVisiblity(bool bShow)
{
	cppEndGameDlgVisiblity = bShow ? ESlateVisibility::Visible : ESlateVisibility::Hidden;
}

