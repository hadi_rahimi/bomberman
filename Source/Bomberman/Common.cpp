#include "Common.h"
#include <math.h>
#include "Global.h"
#include "GameUi.h"

#pragma optimize("", off)

int g_mapMirrorY[TILESX][TILESY] = {
	{ 0, 0, 0, 0, 2, 0, 0, 2, 2, 2, 0, 0, 0 },
	{ 0, 1, 0, 1, 0, 1, 2, 1, 0, 1, 0, 1, 2 },
	{ 2, 2, 0, 0, 0, 0, 2, 0, 0, 2, 0, 2, 2 },
	{ 2, 1, 0, 1, 0, 1, 0, 1, 2, 1, 0, 1, 0 },
	{ 0, 0, 0, 2, 0, 0, 0, 2, 2, 0, 0, 2, 0 },
	{ 2, 1, 2, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0 },
	{ 0, 2, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0 },
	{ 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0 },
	{ 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2 },
	{ 0, 1, 2, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0 },
	{ 0, 0, 0, 2, 0, 0, 0, 0, 2, 0, 0, 0, 0 },
};

int		g_map[TILESX][TILESY];
void*	g_walls[TILESX][TILESY];
void*	g_bombs[TILESX][TILESY];
void*	g_pickups[TILESX][TILESY];

void ResetMapBuffer()
{
	for (int y = 0; y < TILESY; y++) {
		for (int x = 0; x < TILESX; x++) {
			g_map[x][y] = g_mapMirrorY[TILESX - x - 1][y];
		}
	}
}

void GetTileIndex(const FVector& pt, int& x, int& y)
{
	x = floor(pt.X / TILESIZE);
	y = floor(pt.Y / TILESIZE);
}

FVector GetTileCenter(int iX, int iY)
{
	FVector center;
	center.X = (iX * TILESIZE) + TILESIZEDIV2;
	center.Y = (iY * TILESIZE) + TILESIZEDIV2;
	center.Z = 0.0f;

	return center;
}

FVector GetTileCenter(const FVector& pt)
{
	int iX, iY;
	GetTileIndex(pt, iX, iY);

	return GetTileCenter(iX, iY);;
}

