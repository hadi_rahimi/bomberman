#pragma once

#include "CoreMinimal.h"

class UGameUi;

void	GetTileIndex(const FVector& pt, int& x, int& y);
FVector GetTileCenter(int iX, int iY);
FVector GetTileCenter(const FVector& pt);
void	ResetMapBuffer();
